-module(test).

% -import(super, [start/0]).
% -import(client, [start/0]).

-export([start/0]).

start() ->
  % supervisor process, which starts freq server
  super:start(),
  % start client proc
  client:start(),
  % send untrappable exit to kill freq server
  exit(whereis(frequency),kill),
  % freq server will restart
  % start client & it should grab 10 & deallocate etc again...
  timer:sleep(1000),
  client:start().