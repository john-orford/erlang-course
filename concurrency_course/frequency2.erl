%% Based on code from 
%%   Erlang Programming
%%   Francecso Cesarini and Simon Thompson
%%   O'Reilly, 2008
%%   http://oreilly.com/catalog/9780596518189/
%%   http://www.erlangprogramming.org/
%%   (c) Francesco Cesarini and Simon Thompson

-module(frequency2).
-export([start/0,allocate/0,deallocate/1,stop/0, tests/0]).
-export([init/0]).


% Tests

% 1) The way to ensure that it only processes messages that are already in the mailbox, and terminates once they are removed, is to use a timeout of zero. Modify the definition of clear/0 to include this.

% 2) You can simulate the frequency server being overloaded by adding calls to timer:sleep/0 to the frequency server. If these delays are larger than the timeouts chosen for the client code, you will be able to observe the late delivery of messages by modifying clear/0 to print messages as they are cleared from the mailbox. Define a modified version of clear/0 to do this, and test it with your “overloaded” server.


tests() ->
  io:format("Start~n",[]),
  true = start(),
  io:format("Allocate~n",[]),
  cleared_message = allocate(),
  io:format("Deallocate~n",[]),
  cleared_message = deallocate(10),
  io:format("Stop~n",[]),
  stopped = stop(),
  ok.


clear() ->
  receive
    _Msg -> 
      %flag up, and terminate
      cleared_message
  %upon call, immediatly clear message
  after 0 ->
    clear()
  end.


%% These are the start functions used to create and
%% initialize the server.

start() ->
    register(frequency,
	     spawn(frequency2, init, [])).

init() ->
  Frequencies = {get_frequencies(), []},
  loop(Frequencies).

% Hard Coded
get_frequencies() -> [10,11,12,13,14,15].

%% The Main Loop
loop(Frequencies) ->
  receive
    {request, Pid, allocate} ->
      {NewFrequencies, Reply} = allocate(Frequencies, Pid),
      
      %%%%%%%%%%%%%%%%%%%%%%%
      %force delay of 1 sec
      timer:sleep(1000),

      Pid ! {reply, Reply},
      loop(NewFrequencies);
    {request, Pid , {deallocate, Freq}} ->
      NewFrequencies = deallocate(Frequencies, Freq),
      
      %%%%%%%%%%%%%%%%%%%%%%%
      %force delay of 1 sec
      timer:sleep(1000),

      Pid ! {reply, ok},
      loop(NewFrequencies);
    {request, Pid, stop} ->
      Pid ! {reply, stopped}
  end.

%% Functional interface
allocate() -> 
    frequency ! {request, self(), allocate},
    
    receive 
	    {reply, Reply} -> 
        Reply
    
    %%%%%%%%%%%%%%%%%%%%%%%
    %after 500ms, clear up subsequent returns
    after 500 ->
      clear()

    end.

deallocate(Freq) -> 
    frequency ! {request, self(), {deallocate, Freq}},
    receive 
	    {reply, Reply} -> Reply

    %%%%%%%%%%%%%%%%%%%%%%%
    %after 500ms, clear up subsequent returns
    after 500 ->
      clear()

    end.

stop() -> 
    frequency ! {request, self(), stop},
    receive 
	    {reply, Reply} -> Reply
    end.


%% The Internal Help Functions used to allocate and
%% deallocate frequencies.
allocate({[], Allocated}, _Pid) ->
  {{[], Allocated}, {error, no_frequency}};
allocate({[Freq|Free], Allocated}, Pid) ->
  {{Free, [{Freq, Pid}|Allocated]}, {ok, Freq}}.

deallocate({Free, Allocated}, Freq) ->
  NewAllocated=lists:keydelete(Freq, 1, Allocated),
  {[Freq|Free],  NewAllocated}.
