
-module(super).
-export([start/0]).
-export([init/0]).


% 1) Program a supervisor process that can be used to start the frequency server,
% 2) restart it whenever it has terminated unexpectedly.


start() ->
  register(super,
	  spawn(super, init, [])
    ).

init()->
  process_flag(trap_exit, true),
  
  % 1) start frequency server
  register(frequency,
    spawn_link(
      fun() ->
        frequency:init()
        end
      )
    ),

  io:format("Started frequency server~n", []),

  loop().

loop() ->
    receive
      % 2) restart it when an unexpected error occurs
      {'EXIT', From, Reason} ->
        io:format("loop got 'EXIT' from ~p because ~p~n", [From, Reason]),
        %try to ensure all clients die
        init()
    end.