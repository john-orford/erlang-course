
-module(client).
-export([start/0]).
-export([init/0]).

% 3) we can ensure that if the server terminates unexpectedly then the clients do too.


start() ->
	  spawn(client, init, []),
    ok.

init() ->
  process_flag(trap_exit, true),
  %link to server
  link(whereis(frequency)),
  loop().

loop()->
  receive
    % when we receive exit signal, quit
    {'EXIT', From, Reason} ->
      io:format("Server failed, from ~p because ~p~n", [From, Reason]),
      io:format("Shutting down client~n", [])
    after 0 ->
      {ok, F} = frequency:allocate(),
      io:format("Got ~w ~n",[F]),
      
      ok = frequency:deallocate(F),
      io:format("Dealloc'ed ~w ~n",[F]),

      loop()
    end.
