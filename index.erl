-module(index).
-export([indexing_a_file/1]).


% ******************************************************************
% ******************************************************************
% ******************************************************************
% ******************************************************************
% high level steps of the program (wish I had pipes here!)



% e.g. `index:indexing_a_file("gettysburg-address.txt")`
% e.g. `iindex:ndexing_a_file("dickens-christmas.txt")`
% when the text file is in the current directory
indexing_a_file(F) ->
    maps:filter(
        % filter out words with less than a length of 3
        fun(K,_V) -> string:len(K) >= 3 end
        % fold tagged words into a map data structure
        , fold_into_map(
            % reverse the list
            lists:reverse(
                % split lines into words & tag each word with a number
                get_words_and_tag_with_line_numbers(
                    % read file
                    get_file_contents(F)
                )
            )
        )
    ).


% ******************************************************************
% ******************************************************************
% ******************************************************************
% ******************************************************************
% mechanics of the script




get_words_and_tag_with_line_numbers(Xs) ->
    lists:foldl(
        fun( L, [{Ln, _W} | _] = Ws ) -> 
            line_into_words_and_tag(Ln+1, L) ++ Ws
        end
        % this gets filtered out later, thankfully
        , [{0,""}] 
        , Xs
    ).

line_into_words_and_tag(Ln, L) ->
    lists:map(
            fun(W) -> 
                {Ln, W}
            end
        , string:tokens(L, " ,-`:;.'") 
        ).

% ******************************************************************

% create a map from Line Number and Word tuple list
fold_into_map( [ {Ln, W} | Xs] ) ->
    lists:foldl(
        fun( {Ln_, W_}, A ) -> 
            update_map({Ln_, W_}, A)
        end
        , #{W => [Ln]}
        , Xs
    ).

% update map by inserting words and keeping track of lines they appear on
update_map({Ln, W}, A) ->
    maps:update_with(
        string:to_upper(W)
        , fun([{Fst, Snd} | Vs] = Ws) ->
            case Ln - Snd of
                % if Ln is the same, do not add tuple
                0 ->
                    Ws;
                % if Ln is 1 greater, add to Snd number
                1 ->
                    [{Fst, Snd+1} | Vs];
                % else add new tuple to list    
                _ ->
                    [{Ln, Ln} | Ws]
            end
        end
        , [{Ln,Ln}]
        , A
        ).


% ******************************************************************
% ******************************************************************
% ******************************************************************
% ******************************************************************

% provided ---

% Get the contents of a text file into a list of lines.
% Each line has its trailing newline removed.
get_file_contents(Name) ->
    {ok,File} = file:open(Name,[read]),
    Rev = get_all_lines(File,[]),
lists:reverse(Rev).

% Auxiliary function for get_file_contents.
% Not exported.
get_all_lines(File,Partial) ->
    case io:get_line(File,"") of
        eof -> file:close(File),
               Partial;
        Line -> {Strip,_} = lists:split(length(Line)-1,Line),
                get_all_lines(File,[Strip|Partial])
    end.
